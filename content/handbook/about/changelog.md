---
title: Handbook Changelog
description: The last 100 Merge Requests to the Handbook
type: docs
---

{{< mr-list >}}
